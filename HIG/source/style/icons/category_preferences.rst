Category and Preferences Icons
==============================
.. figure:: /img/icon-category.png
   :alt: Colorful category icons

   Category icons

.. figure:: /img/icon-preferences.png
   :alt: Colorful preferences icons

   Preferences icons

Category and Preferences icons are commonly seen in System Settings,
Applications' settings windows, and launcher menus. They always use the
the `colorful style <index.html#monochrome-icon-style>`__ and come in a single
size: 32px. They have 2px margins on the top and bottom:

.. figure:: /img/icon-margins-category.png
   :alt: Margins for 32px category and Preferences icons

   Margins for 32px category and Preferences icons
