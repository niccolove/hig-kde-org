Application Icons
=================
.. figure:: /img/icon-applications.png
   :alt: Colorful category icons

   Application icons

Application icons always use the `colorful style \
<index.html#colorful-icon-style>`__ and come in a single size: 48px. They have
4px margins on the top and bottom:

.. figure:: /img/icon-margins-app.png
   :alt: Colorful Places icons

   Margins for 48px application icons

3rd party app icons
~~~~~~~~~~~~~~~~~~~
When creating a Breeze theme version of an existing app's icon, is critically
important that the icon's existing brand and visual style be preserved. The
goal is to create a Breeze version of the icon, not something completely new
and different. For example:

.. figure:: /img/icon-3rdparty-GIMP.png
   :alt: Breeze version of 3rd-party GIMP icon

   Breeze version of 3rd-party GIMP icon

.. figure:: /img/icon-3rdparty-writer.png
   :alt: Breeze version of 3rd-party LibreOffice Writer icon

   Breeze version of 3rd-party LibreOffice Writer icon
