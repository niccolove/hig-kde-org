Places Icons
============
Places icons are used to depict folders, network locations, and other places.
They come in four sizes: 16px, 22px, 32px, and 64px. Places icons use the
`monochrome style <index.html#monochrome-icon-style>`__ for 16px and 22px sizes,
and the `colorful style <index.html#colorful-icon-style>`__ for 32px and 64px 
sizes.

Monochrome Places icons
~~~~~~~~~~~~~~~~~~~~~~~
.. image:: /img/Breeze-icon-design-places-monochrome.png
   :alt: Small monochrome Places icons

Places icons are monochrome for their 16px and 22px versions. For these
versions, the whole icon depicts the place itself or its typical contents, and
the icon must be perfectly square, with consistent margins on all sides.
Beyond that, simply follow the general monochrome icon guidelines for 16px and
22px icons--but remember that there are are margins on all four sides, not just
the top and bottom.

.. figure:: /img/icon-margins-places-monochrome.png
   :alt: Margins for monochrome places icons

   Margins for 16px and 22px monochrome Places icons

Colorful places icons
~~~~~~~~~~~~~~~~~~~~~
.. image:: /img/Breeze-icon-design-places.png
   :alt: Colorful Places icons

For the colorful versions, the monochrome icon becomes the foreground symbol on
top of a background depicting a folder. The foreground symbol's color is a
darkened version of the background folder's color, and can consist of 1px lines,
or filled-in areas. The foreground symbol should be centered within the folder
background and take up 10x10px for the 32px icon size, and 20x20px for the 64px
size.

Note that for places icons, the foreground symbol does **not** cast a shadow.

.. figure:: /img/icon-margins-places-32.png
   :alt: Margins for monochrome places icons

   Margins for 32px monochrome Places icons

.. figure:: /img/icon-margins-places-64.png
   :alt: Margins for monochrome places icons

   Margins for 64px monochrome Places icons
